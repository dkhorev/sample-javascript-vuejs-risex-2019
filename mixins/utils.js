export default {
  methods: {
    btcAmountToFormat(amount) {
      return parseFloat(amount).toFixed(8);
    },

    divideDate(date) {
      return {
        date: this.$moment(date, 'YYYY-MM-DD HH:mm:ss ZZ').format('L'),
        time: this.$moment(date, 'YYYY-MM-DD HH:mm:ss ZZ').format('LT'),
        dateDay: this.$moment(date, 'YYYY-MM-DD HH:mm:ss ZZ').format('DD MMM'),
      };
    },

    noun(number, one, two, five) {
      let num = Math.abs(number);
      num %= 100;
      if (num >= 5 && num <= 20) {
        return five;
      }
      num %= 10;
      if (num === 1) {
        return one;
      }
      if (num >= 2 && num <= 4) {
        return two;
      }
      return five;
    },

    scrollToPos(position) {
      window.RisexScroll.scrollTo({y: position}, 500); // TODO: find solution without window
    },

    thousandSeparator(str) {
      let nStr = str + '';
      let x = nStr.split('.');
      let x1 = x[0];
      let x2 = x.length > 1 ? '.' + x[1] : '';
      let rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ' ' + '$2');
      }
      return x1 + x2;
      // return str.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
    },

    replaceStringParams(url, data) {
      const regex = new RegExp(':(' + Object.keys(data).join('|') + ')', 'g');

      return url.replace(regex, (m, $1) => data[$1] || m);
    },

    getCryptoAmount(offer, deal) {
      return offer
        ? deal.is_sale
          ? (parseFloat(deal.crypto_amount) + parseFloat(deal.commissions.offer)).toFixed(8)
          : (parseFloat(deal.crypto_amount) - parseFloat(deal.commissions.offer)).toFixed(8)
        : deal.is_sale
          ? (parseFloat(deal.crypto_amount) + parseFloat(deal.commissions.client)).toFixed(8)
          : (parseFloat(deal.crypto_amount) - parseFloat(deal.commissions.client)).toFixed(8);
    },

    floatPlus(...numbers) {
      let count = 0;
      const num = 100000000;
      numbers.forEach(n => count += n * num);
      return parseFloat((count/num).toFixed(8));
    },
    convertStringToNumber(string) {
      return parseFloat(parseFloat(string).toFixed(8));
    }
  },
};
