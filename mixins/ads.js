import utils from './utils';

export default {
  mixins: [
    utils,
  ],

  computed: {
    // implement this.itemsRaw in child component
    items() {
      const adFormatted = this.formatAds(this.itemsRaw);
      return this.formatPaymentSystems(adFormatted);
    },

    // implement this.paginationRaw in child component
    pages() {
      return this.paginationRaw || false;
    },

    hasPagination() {
      return !!this.pages && this.pages.total_pages > 1;
    },
  },

  methods: {
    // implement this.filterAction in child component
    changeCurrent(page) {
      this.$store.dispatch(this.filterAction, { page });
    },

    //
    formatAds(ads) {
      const adsNew = JSON.parse(JSON.stringify(ads));

      return adsNew.map(ad => {
        ad.price = this.thousandSeparator(ad.price);
        ad.min = this.thousandSeparator(ad.min);
        ad.max = this.thousandSeparator(ad.max);
        ad.currency.code = ad.currency.code.toUpperCase();
        ad.crypto_currency.code = ad.crypto_currency.code.toUpperCase();
        ad.created_at = this.divideDate(ad.created_at).date;

        return ad;
      });
    },

    //
    formatPaymentSystems(ads) {
      return ads.map(ad => {
        if (ad.banks && ad.banks.length > 0) {
          ad.payments = ad.banks;
        } else {
          ad.payments = [
            {
              title: ad.payment_system.title,
              icon: ad.payment_system.icon,
            },
          ];
        }

        return ad;
      });
    },
  },
};
